import React, {useState} from 'react';

function ToDoForm(props) {
  const [input, setInput] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault()
    props.addTodo(input)
    setInput("")
  }
  return (
    <form onSubmit={handleSubmit}>
      <input
        type="text"
        value={input}
             onChange={(e) => setInput(e.target.value)}
             placeholder="Add a ToDo"/>
      <button type="submit">Add ToDo</button>
    </form>
  );
}

export default ToDoForm;