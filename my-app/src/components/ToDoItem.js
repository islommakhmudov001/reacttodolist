import React from 'react';
import { RiCloseCircleLine } from "react-icons/ri"

function ToDoItem(props) {
  const { todo, removeTodo} = props
  return (
    <div>
      {props.todo.text}
      <RiCloseCircleLine onClick={() => removeTodo(props.todo.id)}/>
    </div>
  );
}

export default ToDoItem;