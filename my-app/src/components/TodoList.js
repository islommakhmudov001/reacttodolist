import React, {useState} from 'react';
import ToDoForm from "../components/ToDoForm"
import ToDoItem from "../components/ToDoItem";

function TodoList() {
  const [todos, setTodos] = useState([]);

  const addTodo = (text) => {
    let id = 1;
    if (todos.length > 0) {
      id = todos[0].id + 1
    }
    let todo = {id: id, text: text, completed: false}
    let newTodos = [todo, ...todos]
    console.log(newTodos)
    setTodos(newTodos)
  };

  const removeTodo = (id) =>{
   let updatedTodos = [...todos].filter((todo) => todo.id !== id)
    setTodos(updatedTodos)
  }

  return (
    <div className="">
        <h1>Todo List</h1>
      <ToDoForm addTodo={addTodo} />
      {todos.map((todo) => {
        return (
          <ToDoItem removeTodo={removeTodo} todo={todo} key={todo.id}/>
        )
      })}
    </div>
  );
}

export default TodoList;